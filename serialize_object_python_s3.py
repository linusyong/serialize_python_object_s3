#!/usr/bin/python
import boto3
import json

def createBucket(block_public_access = True):
  global session
  s3 = session.client('s3')

  response = s3.create_bucket (
    ACL = 'private',
    Bucket = bucket,
    CreateBucketConfiguration = {
      'LocationConstraint': 'ap-southeast-2'
    }
  )

  if block_public_access:
    response = s3.put_public_access_block (
      Bucket = bucket,
      PublicAccessBlockConfiguration = {
        'BlockPublicAcls': True,
        'IgnorePublicAcls': True,
        'BlockPublicPolicy': True,
        'RestrictPublicBuckets': True
      }
    )

def createPhonebook():
  global session
  s3 = session.client('s3')

  phonebook = dict()

  while True:
    try:
      name = input("Name: ")
      if name.upper() == 'EXIT':
        break
      contact = input("Contact: ")
      phonebook.update({name:contact})

    except Exception as e:
      print(e)

  serializedPhonebook = json.dumps(phonebook)

  s3.put_object(Bucket=bucket,Key='phonebook',Body=serializedPhonebook)

def getPhonebook():
  global session
  s3 = session.client('s3')
  object = s3.get_object(Bucket=bucket,Key='phonebook')
  serializedPhonebook = object['Body'].read()

  return json.loads(serializedPhonebook)

def main():
  global session
  global bucket

  bucket = 'test-linus'

  session = boto3.Session(
    aws_access_key_id = "[aws_access_id]",
    aws_secret_access_key = "[aws_access_key]",
    region_name = "ap-southeast-2"
  )

  createBucket()

  createPhonebook()

  phonebook = getPhonebook()
  print(phonebook)

if __name__ == "__main__":
    main()
